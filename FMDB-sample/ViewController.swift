//
//  ViewController.swift
//
//  Created by i-enter
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var db_data:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.dataSource = self
        tableView.delegate = self

        // 作成したDBファイル（sample-sqlite.db）からDBをセット
        setSqlite()
        
        
//        ///////////////////////////
//        // CREATE TABLE
//        ///////////////////////////
////        let db = FMDatabase(path: _path)
//        let sql_create = "CREATE TABLE IF NOT EXISTS sample (user_id INTEGER PRIMARY KEY, user_name TEXT);"
//        
//        // データベースをオープン
//        db.open()
//        // SQL文を実行
//        let ret = db.executeUpdate(sql_create, withArgumentsIn: [])
//
//        // データベースをクローズ
//        db.close()  
//        
//        if ret {  
//            print("テーブルの作成に成功")
//        }
//        
//        ///////////////////////////
//        // INSERT
//        ///////////////////////////
////        let db = FMDatabase(path: _path)
//        let sql_insert = "INSERT INTO sample (user_id, user_name) VALUES (?, ?);"
//        
//        db.open()
//        // ?で記述したパラメータの値を渡す場合
//        let ret_insert = db.executeUpdate(sql_insert, withArgumentsIn: [1, "sample1"])
//        db.close()
//
//        if ret_insert {
//            print("INSERTに成功")
//        }
//
//        let sql_insert2 = "INSERT INTO sample (user_id, user_name) VALUES (?, ?);"
//        
//        db.open()
//        // ?で記述したパラメータの値を渡す場合
//        let ret_insert2 = db.executeUpdate(sql_insert2, withArgumentsIn: [2, "sample2"])
//        db.close()
//        
//        if ret_insert2 {
//            print("INSERTに成功2")
//        }
//
//        ///////////////////////////
//        // UPDATE
//        ///////////////////////////
////        let db = FMDatabase(path: _path)
//        let sql_update = "UPDATE sample SET user_name = :NAME WHERE user_id = :ID;"
//        
//        db.open()
//        // 名前を付けたパラメータに値を渡す場合
//        let ret_update = db.executeUpdate(sql_update, withParameterDictionary: ["ID":1, "NAME":"Wonderplanet"])
//        db.close()
//
//        if ret_update {
//            print("UPDATEに成功")
//        }

        
        ///////////////////////////
        // DELETE
        ///////////////////////////
//        let db = FMDatabase(path: _path)
//        let sql_delete = "DELETE FROM sample WHERE user_id = ?;"
//        
//        db.open()
//        let ret_delete = db.executeUpdate(sql_delete, withArgumentsIn: [1])
//        db.close()
//
//        if ret_delete {
//            print("DELETEに成功")
//        }

        
//        ///////////////////////////
//        // TRANSACTION
//        ///////////////////////////
////        let db = FMDatabase(path: _path)
//        let sql_insert = "INSERT INTO sample (user_id, user_name) VALUES (?, ?);"
//        
//        var users = [
//                [3, "Aichi"],
//                [4, "Gifu"],
//                [5, "Mie"],
//            ]
//        
//        db.open()
//        // トランザクションの開始
//        db.beginTransaction()
//        
//        var success = true
//        
//        for user in users {
//            // INSERT文を実行
//            success = db.executeUpdate(sql_insert, withArgumentsIn: user)
//            
//            // INSERT文の実行に失敗した場合
//            if !success {
//                // ループを抜ける
//                break
//            }  
//        }  
//        
//        if success {  
//            // 全てのINSERT文が成功した場合はcommit  
//            db.commit()
//            print("commitに成功")
//        } else {
//            // 1つでも失敗したらrollback
//            db.rollback()  
//            print("rollback")
//        }
//        
//        db.close()
        
        
        ///////////////////////////
        // SELECT
        ///////////////////////////
        // FMDatabaseクラスのインスタンスを作成
        // 引数にファイルまでのパスを渡す
        let db = FMDatabase(path: setPath())
        let sql = "SELECT id, name FROM sample ORDER BY id;"
        
        db.open()
        
        let results = db.executeQuery(sql, withArgumentsIn: [])
        
        while (results?.next())! {
            // カラム名を指定して値を取得する方法
            let id = results?.int(forColumn: "id")
            // カラムのインデックスを指定して取得する方法
            let name = results?.string(forColumnIndex: 1)
            
            db_data.append(name!)
            print("id = \(id), name = \(name)")
        }
        
        db.close()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    func numberOfSections(in tableView: UITableView) -> Int { // sectionの数を決める
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // 一つのsectionの中に入れるCellの数を決める。
        return db_data.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dbCell", for: indexPath)
        cell.textLabel?.text = db_data[indexPath.row] // indexPath.rowはセルの番号
        
        return cell
    }
    
//    /**
//     セクション毎のタイトルをヘッダーに表示
//     */
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return foods[section]
//    }

    
    func setPath()->(String){
        // /Documentsまでのパスを取得
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        // <Application>/Documents/sample-sqlite.db というパスを生成
        let dir   = paths[0] as NSString
        let _path = dir.appendingPathComponent("sample-sqlite.db")

        return _path
    }
    
    func setSqlite(){
        if FileManager.default.fileExists(atPath: setPath()) == false {
            //ファイルがない場合はコピー
            let defaltDBPath :String = Bundle.main.path(forResource: "sample-sqlite.db", ofType:nil)!
            
//            print(defaltDBPath)
//            print(setPath())
            
            try! FileManager.default.copyItem(atPath: defaltDBPath, toPath: setPath())
            
            if FileManager.default.fileExists(atPath: setPath()) == false {
                //error
                print("Copy error = " + defaltDBPath)
            } else {
                print("File Copy OK")
            }
        }else{
            //ファイルがある場合は一旦削除してコピー
            try! FileManager.default.removeItem(atPath: setPath())
            let defaltDBPath :String = Bundle.main.path(forResource: "sample-sqlite.db", ofType:nil)!
            try! FileManager.default.copyItem(atPath: defaltDBPath, toPath: setPath())
            
            print("DB replace OK")
        }
        
    }
}

